<?php

namespace App\GraphQL\Query;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use App\User;

/**
 * Class UsersQuery
 * @package App\GraphQL\Query
 * @author Vlad Golubtsov <v.golubtsov@bvblogic.com>
 */
class AddUserQuery extends Query
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'add_user'
    ];

    /**
     * @var User
     */
    protected $user;

    /**
     * AddUserQuery constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return GraphQL\Type\Definition\ListOfType|null
     */
    public function type()
    {
        return Type::listOf(GraphQL::type('User'));
    }

    /**
     * @return array
     */
    public function args()
    {
        return [
            'id'       => ['name' => 'id',       'type' => Type::string()],
            'name'     => ['name' => 'name',     'type' => Type::string()],
            'email'    => ['name' => 'email',    'type' => Type::string()],
            'password' => ['name' => 'password', 'type' => Type::string()],
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return bool
     */
    public function resolve($root, $args)
    {
        $args['password'] = bcrypt($args['password']);

        $user = $this->user;

        $user->fill($args);
        $user->save();

        return [$user];
    }
}