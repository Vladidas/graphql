<?php

namespace App\GraphQL\Query;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use App\User;

/**
 * Class UsersQuery
 * @package App\GraphQL\Query
 * @author Vlad Golubtsov <v.golubtsov@bvblogic.com>
 */
class UsersQuery extends Query
{
    /**
     * @var array
     */
    protected $attributes = [
        'name' => 'users'
    ];

    /**
     * @var User
     */
    protected $user;

    /**
     * AddUserQuery constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return GraphQL\Type\Definition\ListOfType|null
     */
    public function type()
    {
        return Type::listOf(GraphQL::type('User'));
    }

    /**
     * @return array
     */
    public function args()
    {
        return [
            'id'    => ['name' => 'id',    'type' => Type::string()],
            'name'  => ['name' => 'name',  'type' => Type::string()],
            'email' => ['name' => 'email', 'type' => Type::string()]
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function resolve($root, $args)
    {
        if (isset($args['id'])) {
            return $this->user->where('id' , $args['id'])->get();
        } else if(isset($args['email'])) {
            return $this->user->where('email', $args['email'])->get();
        }

        return $this->user->all();
    }
}